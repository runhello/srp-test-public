﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class ShaderResize : MonoBehaviour {
	public Material material;

	// Use this for initialization
	void Start () {
		RenderTextureDescriptor d = XRSettings.eyeTextureDesc;
		d.width = d.width / 2; // THIS DOES NOT MAKE SENSE
		RenderTexture t = new RenderTexture(d);
		GetComponent<Camera>().targetTexture = t;
		material.mainTexture = t;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
