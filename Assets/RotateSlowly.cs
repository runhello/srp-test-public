﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSlowly : MonoBehaviour {
	public float degreeSpeed = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.rotation *= Quaternion.AngleAxis(degreeSpeed, Vector3.up);
	}
}
