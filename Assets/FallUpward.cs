﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallUpward : MonoBehaviour {
	public Vector3 range = new Vector3(10,10,10);
	public Vector2 speedRange = new Vector2(0.1f, 1);
	public float fadeOut = 1;
	public float minSize = 0.01f;
	public float rotScale = 1;

	class ChildState {
		public Transform at;
		public Vector3 v;
		public Vector3 plainScale;
		public Quaternion rotate;
		public ChildState(Transform _at, Vector3 _v, Vector3 _plainScale, Quaternion _rotate) { at = _at; v = _v; plainScale = _plainScale; rotate = _rotate; }
	}
	List<ChildState> children = new List<ChildState>();

	Vector3 randomV() { 
		return new Vector3(0, Random.Range(speedRange.x, speedRange.y), 0);
	}

	Quaternion randomQ() {
		return Quaternion.Lerp(Quaternion.identity, Random.rotation, rotScale);
	}

	// Use this for initialization
	void Start () {
		foreach (Transform child in transform)
		{
			children.Add(new ChildState(child, randomV(), child.localScale, randomQ()));
		}
	}
	
	float ease(float x) {
		return x*x;
	}

	// Update is called once per frame
	void Update () {
		foreach (var child in children) {
			child.at.position += child.v;
			if (child.at.position.y > range.y) {
				child.at.position = new Vector3(Random.Range(-range.x, range.x), -range.y, Random.Range(-range.z, range.z));
				child.v = randomV();
				child.rotate = randomQ();
			}
			float scaleBy = 1;
			if (child.at.position.y > range.y - fadeOut)
				scaleBy = 1 - ease((child.at.position.y - (range.y - fadeOut)) / fadeOut);
			else if (child.at.position.y < -range.y + fadeOut)
				scaleBy = 1 - ease(((-range.y + fadeOut) - child.at.position.y) / fadeOut);
			scaleBy = Mathf.Max(minSize, scaleBy);
			child.at.localScale = child.plainScale * scaleBy;
			child.at.rotation = child.at.rotation * child.rotate;
		}
	}
}
