﻿Shader "Unlit/TVShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 origVertex : POSITION1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.origVertex = v.vertex;
				float4 clipVertex = UnityObjectToClipPos(v.vertex);
				o.vertex = clipVertex;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex); // Unused
				return o;
			}
			
			float2 yflipUv(float2 uv) {
				uv.y = 1 - uv.y;
				return uv;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				float4 clipVertex = UnityObjectToClipPos(i.origVertex);
				// sample the texture
#if UNITY_SINGLE_PASS_STEREO
				fixed4 col = tex2D(_MainTex, UnityStereoTransformScreenSpaceTex(yflipUv( ((clipVertex.xy / clipVertex.w) + 1)/2)) );
#else
				fixed4 col = float4(1,1,0,1);
#endif

				return col;
			}
			ENDCG
		}
	}
}
